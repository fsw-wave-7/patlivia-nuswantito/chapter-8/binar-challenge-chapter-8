import {React, Component} from "react";
import {Container, Col, Row} from "react-bootstrap";
import TablePlayer from "../table/table";
import CreateForm from "../forms/CreateForm";
import Search from "../forms/search";

export default class Player extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [
				{
					username: "Tito Rollis",
					email: "ouvalre@gmail.com",
					password: "asddww",
					experience: 1000,
					lvl: 30,
				},
			],
		};
	}

	// METHOD FOR DELETE
	delete = (e, i) => {
		e.preventDefault();
		let {data} = this.state;
		data.splice(i, 1);
		this.setState({data});
	};

	// METHOD FOR EDIT/UPDATE
	update = (e) => {
		e.preventDefault();
		let {data} = this.state;
		data[parseInt(document.getElementById("id").value) - 1].username = document.getElementById("username").value;
		data[parseInt(document.getElementById("id").value) - 1].email = document.getElementById("email").value;
		data[parseInt(document.getElementById("id").value) - 1].password = document.getElementById("password").value;
		data[parseInt(document.getElementById("id").value) - 1].experience = document.getElementById("experience").value;
		data[parseInt(document.getElementById("id").value) - 1].lvl = document.getElementById("level").value;
		this.setState({data});
	};

	// METHOD FOR ADD/CREATE
	create = (e) => {
		e.preventDefault();
		let {data} = this.state;
		data.push({
			username: document.getElementById("username").value,
			email: document.getElementById("email").value,
			password: document.getElementById("password").value,
			experience: document.getElementById("experience").value,
			lvl: document.getElementById("level").value,
		});
		this.setState({data});
	};

	render() {
		return (
			<Container>
				<Row>
					<Col>
						<Search data={this.state.data}></Search>
					</Col>
				</Row>
				<Row className="mt-5 mb-4">
					<Col>
						<TablePlayer data={this.state.data} delete={this.delete} />
					</Col>
					<Col>
						<CreateForm create={this.create} update={this.update} />
					</Col>
				</Row>
			</Container>
		);
	}
}
