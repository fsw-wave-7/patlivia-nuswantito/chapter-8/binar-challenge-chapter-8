import {React, Component} from "react";
import {Form, Button} from "react-bootstrap";

export default class CreateForm extends Component {
	createOrEdit = (e) => {
		if (parseInt(document.getElementById("id").value) > 0) {
			// EDIT
			return this.props.update(e);
		} else {
			// CREATE
			return this.props.create(e);
		}
	};

	render() {
		return (
			<Form id="form">
				<Form.Group controlId="id">
					<Form.Label>Id</Form.Label>
					<Form.Control type="text" placeholder="Enter Id" />
				</Form.Group>
				<Form.Group controlId="username">
					<Form.Label>Username address</Form.Label>
					<Form.Control type="text" placeholder="Enter Username" />
				</Form.Group>

				<Form.Group controlId="email">
					<Form.Label>Email</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" />
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Password" />
				</Form.Group>
				<Form.Group controlId="experience">
					<Form.Label>Experience</Form.Label>
					<Form.Control type="text" placeholder="Enter experience" />
				</Form.Group>
				<Form.Group controlId="level">
					<Form.Label>Level</Form.Label>
					<Form.Control type="text" placeholder="Enter Level" />
				</Form.Group>
				<Button
					onClick={(e) => {
						this.createOrEdit(e);
					}}
					className="mt-3"
					variant="primary"
					type="submit"
				>
					Submit
				</Button>
			</Form>
		);
	}
}
