import {React, Component} from "react";
import {Form, Button, FormControl} from "react-bootstrap";
import Filtered from "../table/filtered";

export default class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			chosenOne: [
				{
					username: "",
					email: "",
					password: "",
					experience: 0,
					lvl: 0,
				},
			],
		};
	}

	// METHOD FOR SEARCH
	search = (e) => {
		e.preventDefault();
		let data = this.props.data;
		let {chosenOne} = this.state;
		let val = document.getElementById("search").value.toLowerCase();
		let matches = data
			.filter((n) => n.email.toLowerCase() === val || toString(n.experience) === val || toString(n.lvl) === val || n.password.toLowerCase() === val || n.username.toLowerCase() === val)
			.map((playerFilter) => {
				return playerFilter;
			});

		let result =
			data[
				data.findIndex((n) => {
					return n.username.toLowerCase() === matches[0].username.toLowerCase();
				})
			];
		this.setState({chosenOne: [result]});
	};
	render() {
		return (
			<div>
				<Form inline className="d-flex">
					<FormControl id="search" type="text" placeholder="Search Player's Data" className="mr-sm-2" />
					<Button
						type="submit"
						onClick={(e) => {
							this.search(e);
						}}
						variant="outline-success"
					>
						Search
					</Button>
				</Form>
				<Filtered data={this.state.chosenOne}></Filtered>
			</div>
		);
	}
}
