import {React, Component} from "react";
import {Table, Button, Row, Col} from "react-bootstrap";

export default class TablePlayer extends Component {
	render() {
		let data = this.props.data;
		return (
			<div>
				<Row className="mt-4">
					<Col>
						<Table striped bordered hover variant="dark">
							<thead>
								<tr>
									<th>Id</th>
									<th>Username</th>
									<th>Email</th>
									<th>Password</th>
									<th>Experience</th>
									<th>Level</th>
									<th>Manipulate</th>
								</tr>
							</thead>
							<tbody>
								{data.map((n, i) => {
									return (
										<tr>
											<td>{++i}</td>
											<td>{n.username}</td>
											<td>{n.email}</td>
											<td>{n.password}</td>
											<td>{n.experience}</td>
											<td>{n.lvl}</td>
											<td>
												<Button
													onClick={(e, i) => {
														this.props.delete(e, i);
													}}
													variant="danger"
													type="submit"
												>
													Delete
												</Button>
											</td>
										</tr>
									);
								})}
							</tbody>
						</Table>
					</Col>
				</Row>
			</div>
		);
	}
}
