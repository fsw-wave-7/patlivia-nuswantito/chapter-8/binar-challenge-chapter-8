import Header from "./pages/header/Header";
import Player from "./pages/model/player";
import Home from "./pages/home/home";
import {Route, BrowserRouter, Switch} from "react-router-dom";
import {Container, Row, Col} from "react-bootstrap";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
	return (
		<div className="App">
			<BrowserRouter>
				<Container fluid>
					<Header />
				</Container>
				<Row>
					<Col className="mt-5">
						<Switch>
							<Route exact path="/">
								<Home />
							</Route>
							<Route path="/player">
								<Player />
							</Route>
						</Switch>
					</Col>
				</Row>
			</BrowserRouter>
		</div>
	);
}

export default App;
