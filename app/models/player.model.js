module.exports = (sequelize, Sequelize) => {
	const Player = sequelize.define("player", {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			allowNull: false,
			primaryKey: true,
		},
		username: {
			type: Sequelize.STRING,
		},
		email: {
			type: Sequelize.STRING,
		},
		password: {
			type: Sequelize.STRING,
		},
		experience: {
			type: Sequelize.INTEGER,
		},
		lvl: {
			type: Sequelize.INTEGER,
		},
	});

	return Player;
};
